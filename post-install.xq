xquery version "3.1";

declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace xconf="http://exist-db.org/collection-config/1.0";
(: The following external variables are set by the repo:deploy function :)

(: the target collection into which the app is deployed :)
declare variable $target external;

(: we have to test for writable path :)
let $path :=  doc( $target || "/textgrid/data/collection.xconf" )
              //xconf:lucene/xconf:analyzer/xconf:param[@name="mappings"][@type="java.io.FileReader"]
              /substring-before(@value, "/charmap.txt")

let $user := process:execute("whoami", ())//line/text()
let $message1 := $path || " is not available. Create it and make sure "||$user||" can write there."
let $message2 := "Could not write to " || $path || "."
return
if
    (file:is-directory($path))
then
    try { file:serialize-binary( xs:base64Binary(util:base64-encode("test")) , $path||"/beacon.txt") }
    catch * { util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message2 || " &#27;[0m" ) }
else
    util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message1 || " &#27;[0m" )
