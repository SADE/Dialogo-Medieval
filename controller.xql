xquery version "3.1";
(:~ This is the URL rewriting engine.
 :  It routes all requests to the modules and
 :  evaluates the RESTlike pathes.
 :  Set up REST pathes in here!
:)
import module namespace config="https://sade.textgrid.de/ns/config" at "modules/config.xqm";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>

else if ($exist:path eq "/") then
    (: forward root path to index.html :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>
else if (ends-with($exist:resource, ".html")) then
    (: the html page is run through view.xq to expand templates :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/templates{$exist:path}" method="get"/>
        <view>
			<forward url="{$exist:controller}/modules/view.xq">
			    <add-parameter name="exist-resource" value="{$exist:resource}" />
			</forward>
		</view>
		<error-handler>
			<forward url="{$exist:controller}/templates/error.html" method="get"/>
			<forward url="{$exist:controller}/modules/view.xq"/>
		</error-handler>
    </dispatch>
(: Resource paths starting with ~assets are loaded from the sade-assets app :)
else if (contains($exist:path, "/~assets/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/sade_assets/{substring-after($exist:path, '/~assets/')}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>

else if(starts-with($exist:resource, "textgrid")) then
    let $preserveRevision := config:get("textgrid.preserveRevision") = "true"
    let $id :=  if( contains($exist:resource, ".") and $preserveRevision )
                then $exist:resource
                else tokenize($exist:resource, "\.")[1]
    let $isImage := doc(  $config:data-root || "/images.xml" )//image/starts-with(@uri, $id) = true()
    return
        if($isImage)
        then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <redirect url="modules/textgrid/digilib-proxy.xq?id={$id}"/>
            </dispatch>
        else
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="{$exist:controller}/templates/content.html" method="get"/>
                <view>
        			<forward url="{$exist:controller}/modules/view.xq">
        			    <add-parameter name="id" value="{$id}" />
        			    <add-parameter name="exist-resource" value="content.html" />
        			</forward>
        		</view>
        		<error-handler>
        			<forward url="{$exist:controller}/templates/error.html" method="get"/>
        			<forward url="{$exist:controller}/modules/view.xq"/>
        		</error-handler>
            </dispatch>

(: redirect markdown files from documentation to the content.html :)
else if(ends-with($exist:resource, ".md")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="../content.html?id=docs/{$exist:resource}"/>
    </dispatch>
else if($exist:resource = "sitemap.xml") then
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        {for $html in xmldb:get-child-resources( $config:app-root || "/templates")
        where not( contains($html, "_") )
        return
        <url>
            <loc>{request:get-url() => substring-before("sitemap.xml")}{$html}</loc>
        </url>}
    </urlset>
else
    (: everything else is passed through :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>
