xquery version "3.1";

module namespace dialogo="https://sade.textgrid.de/ns/dialogo";

import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console";


declare boundary-space preserve;
declare default element namespace "http://www.tei-c.org/ns/1.0";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xlink="http://www.w3.org/1999/xlink";


declare function dialogo:magic($nodes as node()*){

for $node in $nodes return
typeswitch($node)
    case element(tei:TEI) 
            return 
                element xhtml:div {
                    attribute class {'TEI'},
                    dialogo:magic($node/node())
                }
    case element(tei:teiHeader) return ()
    case element(tei:head)
        return
           (element xhtml:div {
                attribute class {'head'},
                element xhtml:h1 {
                dialogo:magic($node/node())}
                
            }, 
            element xhtml:div {
                attribute class {'infoblock'}, 
                dialogo:modal($node), 
                (<xhtml:a target="_blank" href="raw.html?{request:get-query-string()}"><xhtml:i class="fa fa-file-code-o"></xhtml:i></xhtml:a>)
            })
    case element(tei:lg)
        return
            element xhtml:div {
                attribute class {'row'},
                (
                    (
                if (not($node//tei:lg))
                then
                    element xhtml:div {
                    attribute class {'facs col-sm-6'},
                    dialogo:facs($node)}
                else ()
                    ),
                    element xhtml:div {
                    attribute class {'lg', if (not($node//tei:lg))
                then 'col-sm-6' else ()},
                    dialogo:magic($node/node())}
                )
            }
    case element(tei:l)
        return
            element xhtml:div {
                attribute class {'line'},
                dialogo:magic($node/node())}
    case element(tei:note)
        return
            element xhtml:div {
                attribute class {'note'},
                dialogo:magic($node/node())}
    case text()
        return 
            dialogo:text($node)
    default return dialogo:magic($node/node())
};

declare function dialogo:text($n) {

    element xhtml:span {
        $n/string()
        }

};

declare function dialogo:facs($node) {
try {
    (
let $collection := collection( $config:data-root || '/tile')
let $meta := doc( $node/base-uri() => replace("/data/", "/meta/") )
let $uri := $meta//tgmd:textgridUri/text()

let $s := $uri || '#' || $node/string(@xml:id)
let $link := ($collection//tei:link[ends-with(@targets, $s)])[1]
let $shape := substring-after(substring-before($link/@targets, ' '), '#')
let $doc :=  doc( ($collection//tei:link[ends-with(@targets, $s)]/base-uri())[1]  )

let $image := $doc//svg:image[parent::svg:g/svg:rect/@id = $shape]

let $imguri := $image/@xlink:href
let $imagePath := "textgrid/img/" || substring-after($imguri, "textgrid:") || ".jpg"

let $width  := $image/number(@width)
let $height := $image/number(@height)

let $x := number($doc//svg:g/svg:rect[@id = $shape]/substring-before(@x, '%')) div 100
let $y := number($doc//svg:g/svg:rect[@id = $shape]/substring-before(@y, '%')) div 100

let $w := number($doc//svg:g/svg:rect[@id = $shape]/substring-before(@width, '%')) div 100
let $h := number($doc//svg:g/svg:rect[@id = $shape]/substring-before(@height, '%')) div 100

return
        <svg id="{$node/string(@xml:id)}" viewBox="{$x * $width} {$y * $height} {$w * $width} {$h * $height}" width="100%" height="100%"> 
            <image xlink:href="{$imagePath}" width="{$width}" height="{$height}"/>
            <rect x="{$x * $width}" y="{$y * $height}" width="{$w * $width}" height="{$h * $height}" style="fill:rgba(0,0,255, 0.0);stroke-width:3;stroke:rgb(0,0,0)" />
        </svg>
)
}
catch * {
   ( () )
}

};


declare function dialogo:modal($node as node()*){
<xhtml:i class="fa fa-info" data-toggle="modal" data-target="#myModal"></xhtml:i>,
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title">Informations</h4>
      </div>
      <div class="modal-body">
        <div class="poeminfo">
        Poem:
        <ul>
        {
            
            for $a in $node/following::tei:lg[1]/@*
            return <li>{(local-name($a) || ': ' || string($a))}</li>
            
        }
        </ul><hr/>
        </div>
        { for $lg in $node/following::tei:lg/tei:lg
        return
            <div class="lgInfo">
            Stanza {$lg/string(@n)}:
            <ul>
            {
                
                for $a in $lg/@*[local-name() != 'n'][local-name() != 'id']
                return <li>{(local-name($a) || ': ' || string($a))}</li>
                
            }
            </ul>
            </div>
        }
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
};

declare function dialogo:title($node as node(), $model as map(*), $id) {
if(starts-with($id, "textgrid:"))
then
    (collection( $config:data-root||"/meta" )//tgmd:textgridUri[starts-with(.,$id)]/preceding::tgmd:title/text())[1]
else if(contains($id, "data/"))
then doc( $config:data-root||"/meta/"||substring-after($id, "data/") )//tgmd:title/text()
else $id
    
};