xquery version "3.1";

module namespace app="https://sade.textgrid.de/ns/app";

import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace functx="http://www.functx.com";


declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace tgrel="http://textgrid.info/relation-ns#";

(:~
 : Get the title of this app by using the config.xml.
 : @param $node the HTML node with the attribute which triggered this call
 : @param $model a map containing arbitrary data - used to pass information between template calls
 :)
declare function app:title($node as node(), $model as map(*)) {
    config:get("project-title")
};

(:~
 : Provides dynamic CSS based on the requested resource name :)
declare function app:css-injection($node as node(), $model as map(*), $exist-resource) {
    switch ($exist-resource)
        case "publish.html" return <link href="~assets/publish-gui/publish-gui.css" rel="stylesheet"/>
        case "content.html" return
                (<link href="~assets/TEI-Stylesheets/tei.css" rel="stylesheet"/>,
                 <link href="templates/css/content.css" rel="stylesheet"/>)
        default return ()
};

(:~
 : Provides dynamic JavaScripts based on the requested resource name :)
declare function app:javascript-injection($node as node(), $model as map(*), $exist-resource) {
    switch ($exist-resource)
        case "publish.html" return <script src="~assets/publish-gui/publish-gui.js"/>
        default return ()
};

(:~
 : serves a random fontawesome icon :
 :)
declare function app:icon($node as node(), $model as map(*)) {
let $icons := ("bath", "bicycle", "coffee", "heart")
let $random := util:random( count($icons) ) + 1
return
    element {local-name($node)} {
        $node/@*
            [not( starts-with(local-name(), "data") )]
            [not( local-name() = "class" )],
        attribute class {
            "fa",
            "fa-"||$icons[$random]
        }
}

};

(:~
 :
 :)
declare function app:recentlyPublished($node as node(), $model as map(*), $howmany) as map(*)* {
let $collection-uri := $config:app-root || "/" || config:get("project-id") || "/meta"
return if( not(xmldb:collection-available($collection-uri)) ) then () else
let $last-resources :=
    for $resource in xmldb:get-child-resources($collection-uri)
    let $last-modified := xmldb:last-modified($collection-uri, $resource)
    order by $last-modified descending
    return
        $resource

let $metadata := $last-resources[1,2,3] ! doc( $collection-uri || "/" || . )
return
    map { "last-resources": $metadata[1,2,3] }
};

declare function app:recentlyPublished-link($node as node(), $model as map(*), $num as xs:integer) {
<a href="./{$model("last-resources")[$num]//tgmd:textgridUri/string()}">
    <img class="media-object" src="~assets/generic/icons/{replace($model("last-resources")[$num]//tgmd:format, "/", "-")}.svg" alt="{string($model("last-resources")[$num]//tgmd:format)}"/>
</a>
};

declare function app:recentlyPublished-title($node as node(), $model as map(*), $num as xs:integer) {
    $model("last-resources")[$num]//tgmd:title/text()
};

declare function app:recentlyPublished-description($node as node(), $model as map(*), $num as xs:integer) {
let $lastModinLab := $model("last-resources")[$num]//tgmd:lastModified/substring-before(., ".")
let $info := if($lastModinLab = "") then () else
    "This document was last modified in the Lab at "
    || $lastModinLab
    || "."

let $root-name := ($model("last-resources")[$num]//tgrel:rootElementLocalPart/string(.))[1]
let $namespace := ($model("last-resources")[$num]//tgrel:rootElementNamespace/string(@rdf:resource))[1]

let $additionalInfo :=  if($root-name = "") then () else
        "Its root element ("
    || $root-name
    || ") is in the »"
    || $namespace
    || "« namespace."

return
  $info || $additionalInfo
};

declare
    %templates:wrap
function app:featuredWorks($node as node(), $model as map(*)) {
let $collection-uri := $config:app-root || "/" || config:get("project-id") || "/meta"
return if( not(xmldb:collection-available($collection-uri)) ) then () else
let $largest-resources :=
    for $extent in collection($collection-uri)//tgmd:extent
    order by number($extent/text()) descending
    return
        $extent/ancestor::tgmd:MetadataContainerType

return
    map {
        "largest-resources": $largest-resources[1,2,3,4],
        "largest-extent": $largest-resources[1]//tgmd:extent/string()
        }
};

declare function app:featuredWorks-a($node as node(), $model as map(*)) {
    element { node-name($node) } {
                    $node/@*[local-name() != "href"],
                    attribute href {"./"||$model("largest")//tgmd:textgridUri/string()},
                    templates:process($node/node(), $model)
            }
};
declare function app:featuredWorks-img($node as node(), $model as map(*)) {
    element { node-name($node) } {
        attribute src {"~assets/generic/icons/"||replace($model("largest")//tgmd:format, "/", "-")||".svg"},
        attribute alt {$model("largest")//tgmd:format/string()}
    }
};
declare function app:featuredWorks-title($node as node(), $model as map(*)) {
    element { node-name($node) } {$model("largest")//tgmd:title/string()}
};
declare function app:featuredWorks-progress($node as node(), $model as map(*)) {
    element { node-name($node) } {
        attribute max { $model("largest-extent") },
        attribute value { $model("largest")//tgmd:extent/string() }
    }
};
declare function app:featuredWorks-intro($node as node(), $model as map(*)) {
    element { node-name($node) } {
        (doc($model("largest")/replace(base-uri(), "/meta/", "/data/"))//tei:text//text())[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    }
};

declare function app:getLanguage() {
    let $http-header := request:get-header("Accept-Language")
    let $http-header := functx:substring-before-if-contains($http-header,"-")
    let $http-header := functx:substring-before-if-contains($http-header,";")
    let $http-header := functx:substring-before-if-contains($http-header,",")
    let $param-lang := request:get-parameter("lang", "")
    let $lang := if ($param-lang != "") then ($param-lang) else ($http-header)
    let $langConfigured := (config:get("lang.default", "multilanguage"), tokenize(config:get("lang.alt", "multilanguage"), ";"))
    let $result := if( $lang = $langConfigured ) then $lang else $langConfigured[1]

    return ($result)
};

declare function app:getAllLanguages() {
    (
        config:get("lang.default", "multilanguage"),
        config:get("lang.alt", "multilanguage") => tokenize(";")
    )
};

declare function app:switchLanguage($node as node(), $model as map(*), $to as xs:string) as node() {
let $query-string := (
    tokenize(
        request:get-query-string(), "&amp;"
    )[not(starts-with(., "lang="))],
    "lang="||$to)
return
element { name($node) } {
    attribute href { "?" || string-join($query-string, "&amp;") },
    $node/node()
}
};

(:~
 : link rewriter. takes a URL and returns the URL including the lang
 : parameter.
 :   :)
declare
    %templates:wrap
function app:rewriteLink($ref as xs:string) as xs:string {
let $lang := app:getLanguage()
let $base-url := tokenize($ref, "\?|#")[1],
    $get-parameter := tokenize(substring-after($ref, "?"), "#")[1],
    $get-parameter :=   if(contains($get-parameter, "lang="))
                        then $get-parameter
                        else
                            (tokenize($get-parameter, "&amp;")[.!=""],
                            "lang="||$lang),
    $anchor := substring-after($ref, "#")

return
    $base-url
    || "?" || string-join($get-parameter, "&amp;")
    || (if($anchor != "") then "#" || $anchor else ())
};

declare
%templates:wrap
function app:list-docs($node as node(), $model as map(*)) {
    for $item in xmldb:get-child-resources( $config:app-root || "/docs" )
    return
        <li>{$item}</li>
};

(:~
 : serves the error messages powered by programmingexcuses.com
 : a random message is created on each error report and send out to the user.
 :   :)
declare function app:error($node as node(), $model as map(*)) {
    httpclient:get(xs:anyURI('http://programmingexcuses.com/'), false(), ())//*:body/*:div[@class="wrapper"]/*:center/*:a/text()
};

declare function app:currentyear($node as node(), $model as map(*)) {
    year-from-date( current-date() )
};
