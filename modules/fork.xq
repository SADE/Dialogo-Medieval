xquery version "3.1";
(:~
 : This script generates a copy of the current application, renames it and
 : deploys the package into the same database.
 : Afterwards the client is redirected to the same page the request started
 : but in the new application.
 :
 : @author Mathias Göbel
 : @version 0.1
 : :)

import module namespace dbutil="http://exist-db.org/xquery/dbutil";
import module namespace config="https://sade.textgrid.de/ns/config" at "/db/apps/sade/modules/config.xqm";

declare namespace cf="https://sade.textgrid.de/ns/configfile";
declare namespace expath="http://expath.org/ns/pkg";
declare namespace repo="http://exist-db.org/xquery/repo";

declare variable $target := request:get-parameter("target", "koma");
declare variable $referer := request:get-header("Referer");
declare variable $dry-run := request:get-parameter("dry", "false");

declare variable $expathConf :=
    <package xmlns="http://expath.org/ns/pkg" name="https://sade.textgrid.de/ns/SADE/{$target}" abbrev="SADE-{$target}" version="0.1" spec="1.0">
        <title>{$target}</title>
        <dependency package="http://exist-db.org/apps/shared"/>
        <dependency package="https://sade.textgrid.de/ns/assets"/>
    </package>;

declare variable $repoConf :=
    <meta xmlns="http://exist-db.org/xquery/repo">
        <description>{$target}. A Fork of TextGrid: SADE.</description>
        <author>Johannes Biermann</author>
        <author>Mathias Göbel</author>
        <author>Markus Matoni</author>
        <author>Ubbo Veentjer</author>
        <website>https://sade.textgrid.de</website>
        <status>alpha</status>
        <license>GNU-LGPL</license>
        <copyright>true</copyright>
        <type>application</type>
        <target>sade-{$target}</target>
        <permissions password="sade{$target}" user="{$target}" group="sade{$target}" mode="rw-r--r--"/>
        <prepare>pre-install.xq</prepare>
        <finish>post-install.xq</finish>
    <deployed>{current-dateTime()}</deployed>
    </meta>;


declare function local:xar($app-collection as xs:string, $expand-xincludes as xs:boolean) {
    let $entries :=
        dbutil:scan(xs:anyURI($app-collection), function($collection as xs:anyURI?, $resource as xs:anyURI?) {
            let $resource-relative-path := substring-after($resource, $app-collection || "/")
            let $collection-relative-path := substring-after($collection, $app-collection || "/")
            return
                if (empty($resource)) then
                    (: no need to create a collection entry for the app's root directory :)
                    if ($collection-relative-path eq "") then
                        ()
                    else
                        <entry type="collection" name="{$collection-relative-path}"/>
                else if (util:binary-doc-available($resource)) then
                    <entry type="uri" name="{$resource-relative-path}">{$resource}</entry>
                else
                    <entry type="xml" name="{$resource-relative-path}">{
                        util:declare-option("exist:serialize", "expand-xincludes=" || (if ($expand-xincludes) then "yes" else "no")),
                        doc($resource)
                    }</entry>
        })
    let $xar := compression:zip($entries, true())
    return
        $xar
};

if( $target = "" ) then error( QName("https://sade.textgrid.de/ns/error", "FORK01"), "No target provided.") else

let $expand-xincludes := request:get-parameter("expand-xincludes", "false") cast as xs:boolean
let $name := string($expathConf/@abbrev)
let $xar-name := $name || "-" || string($expathConf/@version) || ".xar"
let $source-collection := if(starts-with($config:app-root, "null")) then substring-after($config:app-root, "null") else $config:app-root
let $source-name := tokenize($source-collection, "/")[last()]
let $login := xmldb:login(
        "/db",
        request:get-parameter("forkUser", ""),
        request:get-parameter("forkPassword", "")
    )
return
    if(not($login))
    then error( QName("https://sade.textgrid.de/ns/error", "FORK03"), "Authentication not successful. :-(")
    else

let $prepare as xs:string :=
    (
        xmldb:create-collection(xmldb:create-collection("/db", "tmp"), $name),
        xmldb:copy($source-collection, "/db/tmp/"||$name)
    )
let $replace-the-configs := (
    xmldb:store("/db/tmp/"||$name ||"/"||$source-name, "repo.xml", $repoConf),
    xmldb:store("/db/tmp/"||$name ||"/"||$source-name, "expath-pkg.xml", $expathConf))
let $edit-config.xml :=
    update replace doc( "/db/tmp/"||$name ||"/"||$source-name||"/config.xml" )//cf:param[@key = "project-title"]/text() with text {replace($target, "\-", " ")}
let $path-to-xar as xs:string :=
    xmldb:store-as-binary(
        "/db/tmp/"||$name,
        $xar-name,
        xs:base64Binary(local:xar( $prepare||"/"||$source-name, $expand-xincludes))
    )
return
    if($dry-run = "true")
    then
        (: this error is not an error, but in case of a dry run, we want to get
        a status information, that tells us, that it was a dry run. :)
        error( QName("https://sade.textgrid.de/ns/error", "FORK02"), "Dry run completed.")
    else
        (: we are using the repo addon because it handles all permissions
           correct. :)
        let $complete := repo:install-and-deploy-from-db($path-to-xar)

return
    if( $complete/@result = "ok" )
    then
        let $uri := replace($referer, "apps/.+/publish\.html", "apps/"||replace($name, "SADE", "sade")||"/publish.html")
        return response:redirect-to(xs:anyURI($uri))
    else $complete
